Jeu de l'oie

COMPIEGNE Lucas
MIKOU Youssef

//////////////////////////////////////// Commandes
Commande pour compiler le projet : javac -sourcepath src -d classes src/goosegame/*.java

Commande pour éxécuter le main avec 4 joueurs : java -classpath classes goosegame.jeuDeLoie 4

Commande pour run les tests : javac -classpath test4poo.jar test/goosegame/*.java
java -jar test4poo.jar goosegame.testGame

Commande pour la doc : javadoc -sourcepath src -d docs -subpackages goosegame

Création du jar : jar cvfe appli.jar goosegame.jeuDeLoie -C classes .

Exécution du jar : java -jar appli.jar 4

///////////////////////////////////////
Objectif du TP : utiliser les classes abstraites et nous faire développer un code sans trop d'indication


Difficultés : 0

Remarque : le code peut surement être simplifier et optimiser.
