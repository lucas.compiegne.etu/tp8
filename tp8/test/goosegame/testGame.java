package goosegame;

import static org.junit.Assert.*;

import java.beans.Transient;

import org.junit.Before;
import org.junit.Test;

public class testGame{

    private Cell c1;
    private Cell c2;

    private Player p1;
    private Player p2;

    private Board b1;

    private Game g1;

    @Before
    public void before(){
        this.b1 = new ClassicBoard();
        this.g1 = new Game(b1);

        this.c1 = new Cell(25);
        this.c2 = new Cell(30);

        this.p1 = new Player("name");
        this.p2 = new Player("name2", c1);
    }

    @Test
    public void testAddPlayer(){
        this.g1.addPlayer(this.p1);
    }

    @Test
    public void testCell(){
        this.g1.addPlayer(this.p2);
        assertSame(this.p2.getCell(), c1);
        assertTrue(this.p2.getCell().isEquals(c1));
        this.p2.setCell(c2);
        assertNotSame(this.p2.getCell(), c1);
        assertSame(this.p2.getCell(), c2);
    }


    // ---Pour permettre l'execution des tests ----------------------
    public static junit.framework.Test suite() {
    return new junit.framework.JUnit4TestAdapter(goosegame.testGame.class);
 }
}