package goosegame;

public class ClassicBoard extends Board{
    /** tab that contains the case oie */
    private Cell[] caseOie;
    /** tab that contains the case Piege */
    private Cell[] casePiege;
    /** tab that contains the case Attente */
    private Cell[] caseAttente;
    /** tab that contains the case TP */
    private Cell[] caseTP;

    /**
     * init a classic board
     */
    public ClassicBoard() {
        super(63);

        this.caseOie = new Cell[6];
        this.casePiege = new Cell[2];
        this.caseAttente = new Cell[1];
        this.caseTP = new Cell[3];
    }

    /**
     * init all the custom cells
     */
    public void initBoard(){
        for(int i = 0; i < this.caseOie.length; i++){
            if(i == 0) this.caseOie[i] = new Cell(9);
            else this.caseOie[i] = new Cell(9*(i+1), "Case Oie", true);
        }

        this.casePiege[0] = new Cell(31, "Le puit", false);
        this.casePiege[1] = new Cell(52, "Prison", false);

        this.caseAttente[0] = new Cell(19, "Case Attente", false);
        
        this.caseTP[0] = new Cell(6, "Cheval", true);
        this.caseTP[1] = new Cell(42, "Labyrinthe", true);
        this.caseTP[2] = new Cell(58, "Tête de mort", true);
    }

    /**
     * 
     * @return case oie tab
     */
    public Cell[] getTabCaseOie(){
        return this.caseOie;
    }

    /**
     * 
     * @return case piege tab
     */
    public Cell[] getTabCasePiege(){
        return this.casePiege;
    }

    /**
     * 
     * @return case attente tab
     */
    public Cell[] getTabCaseAttente(){
        return this.caseAttente;
    }

    /**
     * 
     * @return case tp tab
     */
    public Cell[] getTabCaseTP(){
        return this.caseTP;
    }
}
