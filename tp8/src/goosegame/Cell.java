package goosegame;

public class Cell {
    /** cell number */
    private int cellNb;

    /** allows us to know if the cell can be left */
    private boolean canBeLeft;

    /** name of the cell */
    private String name;

    /**
     * constructor
     * @param cellNb cell number
     * @param name name of he cell
     * @param canBeLeft if the cell can be left or not
     */
    public Cell(int cellNb, String name, boolean canBeLeft){
        this.cellNb = cellNb;
        this.canBeLeft = canBeLeft;
        this.name = name;
    }

    /**
     * for a random cell
     * @param cellNb cell number
     */
    public Cell (int cellNb){
        this(cellNb, null, true);
    }

    /**
     * 
     * @return the cell's number
     */
    public int getCellNb(){
        return this.cellNb;
    }
    
    /**
     * 
     * @return if the cell can be left
     */
    public boolean getCanBeLeft(){
        return canBeLeft;
    }

    /**
     * 
     * @return the case name
     */
    public String getCaseName(){
        return this.name;
    }

    /**
     * 
     * @param c 
     * @return if the cell is equal to an other
     */
    public boolean isEquals(Object c){
        if (c instanceof Cell) {
			Cell theOther = ((Cell) c);
			return this.cellNb == theOther.cellNb;
					
		} else {
			return false;
		}
    }
}
