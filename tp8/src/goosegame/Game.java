package goosegame;

import java.util.*;

public class Game {
    /** Player list */
    private List<Player> thePlayers;
    /** board */
    private Board board;
    /** used to know how many turns players are blocked */
    private int[] isBlocked;

    /**
     * constructor
     * @param board
     */
    public Game(Board board){
        this.thePlayers = new ArrayList<Player>();
        this.board = board;
        this.board.initBoard();
        
        this.isBlocked = new int[100];
    }

    /**
     * add player to the game
     * @param p
     */
    public void addPlayer(Player p){
        this.thePlayers.add(p);
    }

    /**
     * fonction in order to play the game
     */
    public void play(){
        boolean V = false;

        Cell[] caseOie = this.board.getTabCaseOie();
        Cell[] caseAttente = this.board.getTabCaseAttente();
        Cell[] casePiege = this.board.getTabCasePiege();
        Cell[] caseTP = this.board.getTabCaseTP();

        for(Player p1 : this.thePlayers){
            //Vérifie si personne n'a gagné la partie
            if(p1.getCell().getCellNb() == this.board.getNbOfCells()){
                System.out.println(p1.toString() + " has won.");
                V = true;
                return;
            }
        }

        //vérifie si aucun gagnant
        if(V == false){
            for(Player p : this.thePlayers){
                if(p.getCell().getCellNb() == this.board.getNbOfCells()){
                    break;
                }
                else if(p.getCell().getCellNb() != this.board.getNbOfCells()){
                    Suite: for(Player gamePlayer : this.thePlayers){
                        //Lance les dés
                        int resultDice = gamePlayer.twoDiceThrow();
                        int actualCell = gamePlayer.getCell().getCellNb();
                        int r = actualCell+resultDice;

                        //vérifie s'il y a quelqu'un sur la case
                        for(Player p2 : this.thePlayers){
                            if(r == p2.getCell().getCellNb()){
                                if(gamePlayer.toString() == p2.toString()){
                                    break;
                                }
                                else{  
                                    gamePlayer.setCell(new Cell(r));
                                    p2.setCell(new Cell(r-1));
                                    System.out.println(gamePlayer.toString() + " a remplacé : " + p2.toString() + " et est à la case : " + r);  
                                }
                                
                            }
                        }

                        //Vérifie si on est à la fin du jeu
                        if(r > this.board.getNbOfCells()){
                            int changeCell = r - this.board.getNbOfCells();
                            int rTemp = this.board.getNbOfCells() - changeCell;
                            gamePlayer.setCell(new Cell(rTemp));
                            System.out.println(gamePlayer.toString() + " a avancé de : " + changeCell + " et est à la case : " + rTemp);
                        }

                        
                        //Tous les autres cas
                        else{
                            //Vérifie s'il est une case piege
                            for(int k = 0; k < casePiege.length; k++){
                                switch(gamePlayer.getCell().getCellNb()){
                                    case 52:
                                        gamePlayer.setCell(new Cell(52));
                                        System.out.println(gamePlayer.toString() + " est bloqué dans la case 'Prison' " + gamePlayer.getCell().getCellNb());
                                        continue Suite;
                                    case 31:
                                        gamePlayer.setCell(new Cell(31));
                                        System.out.println(gamePlayer.toString() + " est bloqué dans la case 'Le Puit' " + gamePlayer.getCell().getCellNb());
                                        continue Suite;
                                    }
                            }

                            //Vérifie la case attente
                            for(int l = 0; l < caseAttente.length; l++){
                                int a = this.thePlayers.indexOf(gamePlayer);
                                if(this.isBlocked[a] == 1 || this.isBlocked[a] == 2 || this.isBlocked[a] == 3) { 
                                    switch(this.isBlocked[a]){
                                        case 1:
                                            gamePlayer.setCell(new Cell(r));
                                            this.isBlocked[a] = 0;
                                            continue Suite;
                                        case 2:
                                            this.isBlocked[a] = 1;
                                            System.out.println(gamePlayer.toString() + " est bloqué dans la case attente : " + gamePlayer.getCell().getCellNb() + " pour 1 tours."); 
                                            continue Suite;
                                        case 3:
                                            this.isBlocked[a] -= 1;
                                            System.out.println(gamePlayer.toString() + " est bloqué dans la case attente : " + gamePlayer.getCell().getCellNb() + " pour 2 tours."); 
                                            continue Suite;
                                    }
                                }
                                if(gamePlayer.getCell().getCellNb() == 19){
                                    System.out.println(gamePlayer.toString() + " est bloqué dans la case attente : " + gamePlayer.getCell().getCellNb() + " pour 2 tours.");  
                                    this.isBlocked[a] = 3;
                                    continue Suite;
                                }
                            }

                            gamePlayer.setCell(new Cell(r));
                            System.out.println(gamePlayer.toString() + " lance " + resultDice + "  et arrive à la case : " + gamePlayer.getCell().getCellNb());

                            //S'occupe des cases oies 
                            for(int i = 0; i < caseOie.length; i++){
                                if(gamePlayer.getCell().isEquals(caseOie[i])){
                                    int temp = gamePlayer.getCell().getCellNb() + resultDice;
                                    System.out.println(gamePlayer.toString() + " arrive à la case Oie : " + gamePlayer.getCell().getCellNb() + " et donc fini sur la case " + temp);
                                    gamePlayer.setCell(new Cell(temp));
                                    break;
                                }
                            }
                            
                            //S'occupe des cases TP
                            for(int l = 0; l < caseTP.length; l++ ){
                                if(gamePlayer.getCell().isEquals(caseTP[l])){
                                    switch(gamePlayer.getCell().getCellNb()){
                                        case 6:
                                            System.out.println(gamePlayer.toString() + " arrive à la case Cheval (6) et donc fini à la case : " + 12);
                                            gamePlayer.setCell(new Cell(12));
                                            break;
                                        
                                        case 42:
                                            System.out.println(gamePlayer.toString() + " arrive à la case Labyrinthe (42) et donc fini à la case : " + 30);
                                            gamePlayer.setCell(new Cell(30));
                                            break;

                                        case 58:
                                            System.out.println(gamePlayer.toString() + " arrive à la case Tête de mort (58) et donc fini à la case : " + 1);
                                            gamePlayer.setCell(new Cell(1));
                                            break;
                                    }
                                }
                            }         
                        }
                    }
                }
            }
        } 
        this.play(); 
    }   
}