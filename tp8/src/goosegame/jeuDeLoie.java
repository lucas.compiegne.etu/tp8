package goosegame;

public class jeuDeLoie {
    public static void main(String[] args){
        Board b = new ClassicBoard();
        Game g = new Game(b);
        Cell depart = new Cell(0);

        for(int i = 1; i < Integer.parseInt(args[0]) + 1; i++){

            g.addPlayer(new Player("Joueur " + String.valueOf(i), depart));
        }

        g.play();
    }   
}
