package goosegame;

//Faire classe absrait (et Cell doit être une interface(pas sur)) donc Board extends Cell et ClassicBoard extends Board et faire méthode de cells dans ClassicBoard

public abstract class Board {

    /** Number of cells in the board */
    private final int nbOfCells;
    /** Tab that contains all the cells */
    private Cell[] theCells;

    /** constructor
      *  @param nbOfCells
      */
    public Board(int nbOfCells){
        this.nbOfCells = nbOfCells;
        this.theCells = new Cell[nbOfCells+1];

        for(int i = 0; i < nbOfCells+1; i++){
            this.theCells[i] = new Cell(i);
        }
    }    

    /**
     * @param index
     * @return the Cell according to the index
     */
    public Cell getCell(int index){
        return this.theCells[index];
    }

    /**
     * @return the number of cells
     */
    public int getNbOfCells(){
        return this.nbOfCells;
    }

    /**
     * Init the board
     */
    public abstract void initBoard();

    /**
     * @return the cells named case oie
     */
    public abstract Cell[] getTabCaseOie();

    /**
    * @return the cells named case piege
    */
    public abstract Cell[] getTabCasePiege();

    /**
     * @return the cells named case attente
     */
    public abstract Cell[] getTabCaseAttente();

    /**
     * @return the cells named case téléportation
     */
    public abstract Cell[] getTabCaseTP();
}
